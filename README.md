# Python图书管理系统

### 注: excel模板是用的七牛云下载的，会到期！在文件夹books/excel模板中找模板文件

## 前端

- 技术选型

  - Vue + ElementUi

- 文件目录

  - vue-admin-template

- 运行

  ```shell
  - npm install # 下载依赖 
  - npm run dev # 运行项目
  ```

 - 运行端口 
   - 80

## 后端

- 技术选型

  - Django

- 文件目录

  - books

- 运行

  ```shell
  - run.bat
  - 或 python manage.py runserver 8888
  ```

 - 运行端口 
   - 8888

## 数据库

MySQL 8.0

在books\config\settings.py中更改数据库配置
![输入图片说明](img/image-20220616181229156.png)

## 运行界面

访问地址: localhost:80

### 登录:

用户名: admin

密码: admin
![输入图片说明](img/image-20220616180401415.png)

### 注册: 

![输入图片说明](img/image-20220616180439022.png)

### 用户增删改查页面
![输入图片说明](img/image-20220616180520227.png)

### 图书增删改查/批量添加以及模糊搜索

![输入图片说明](img/image-20220616180549457.png)

### 借阅界面
![输入图片说明](img/image-20220616180612076.png)
# 任何问题都可加我

## QQ: 1468430721 免费咨询

## 有意一起做项目也可加我，一起进步！